import 'package:app/src/providers/flower_classification_camera_view_model.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class FlowerClassificationCameraScreen extends StatelessWidget {

  FlowerClassificationCameraScreen({Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FlowerClassificationCameraViewModel>.reactive(
      viewModelBuilder: () => FlowerClassificationCameraViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(title: Text("Flower Classification"),),
          body: model.bodyScreen
        );
      }
    );
  }
}