import 'dart:convert';
import 'dart:io';

import 'package:app/src/services/flower_classification_service.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class FlowerClassificationCameraViewModel extends BaseViewModel{

  late BuildContext _context;
  late Size _screenSize;
  late CameraController _cameraController;
  late XFile _image;
  Widget _bodyScreen = Container();

  Size get screenSize => _screenSize;
  Widget get bodyScreen => _bodyScreen;
  CameraController get cameraController => _cameraController;

  initialize(BuildContext ctx) async {
    setBusy(true);
    this._context = ctx;
    this._screenSize = MediaQuery.of(this._context).size;
    var cameras = await availableCameras();
    this._cameraController = CameraController(
      cameras[0],
      ResolutionPreset.medium,
    );
    await this.setCameraPreview();
    setBusy(false);
    notifyListeners();
  }

  takePicture() async{
    this._image = await this._cameraController.takePicture();
    final response = await FLowerClassificationService.predict(this._image.path, this._image.name);
    final respBody = response.body;
    final respMap = jsonDecode(respBody) as Map<String, dynamic>;
    await this.setBodyImagePreview(respMap['class_name']);
    notifyListeners();
  }

  setBodyImagePreview(String flowerClass) async{
    this._bodyScreen = Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: this._screenSize.height*0.03),
              width: this._screenSize.width*0.9,
              height: this._screenSize.height*0.5,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Image.file(File(this._image.path), ).image,
                  fit: BoxFit.contain
                )
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(top: this._screenSize.height*0.3),
              child: Text(flowerClass, style: TextStyle(fontSize: this._screenSize.width*0.08, fontWeight: FontWeight.bold),)
            )
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: this._screenSize.height*0.02),
              child: FloatingActionButton(
              child: Icon(Icons.camera_alt_outlined),
              onPressed: () async {
                await this.setCameraPreview();
                notifyListeners();
              }
            ),
            )
          )
        ],
      )
    );
  }

  setCameraPreview() async {
    await this._cameraController.initialize();
    this._bodyScreen = Container(
      child: Column(
        children: [
          CameraPreview(this._cameraController),
          Center(
            child: Container(
            margin: EdgeInsets.only(top: this._screenSize.height*0.03
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: FloatingActionButton(
                    child: Icon(Icons.camera),
                    onPressed: (){
                      this.takePicture();
                    }
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: this._screenSize.width*0.03),
                  child: FloatingActionButton(
                    child: Icon(Icons.flash_off),
                    onPressed: (){
                      this._cameraController.setFlashMode(FlashMode.off);
                    }
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: this._screenSize.width*0.03),
                  child: FloatingActionButton(
                    child: Icon(Icons.flash_auto),
                    onPressed: (){
                      this._cameraController.setFlashMode(FlashMode.auto);
                    }
                  ),
                ),
              ],
            ),
          ),
          )
        ],
      ),
    );
  }

}