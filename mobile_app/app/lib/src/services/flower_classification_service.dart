import 'package:http/http.dart' as http;

class FLowerClassificationService {

  static String get host => "http://192.168.0.19:2404";

  static Future<http.Response> predict(String filePath, fileName) async {
    Uri uri = Uri.parse(host+"/flower-classification/predict-pict");
    var request = new http.MultipartRequest("POST", uri);

    var multipartFile = await http.MultipartFile.fromPath(
      'file',
      filePath,
      filename: fileName
    );
    request.files.add(multipartFile);
    var responseStream = await request.send();
    var response = await http.Response.fromStream(responseStream);

    return response;
  }

}