from fastapi import FastAPI
from src.delivery import server
import tensorflow as tf
import src.ml_model as ml_model

app = FastAPI()

server.run_web_server()