from typing import Any
from src.pkg.mechine_learning_util import MechineLearningUtil
from tensorflow.keras.models import Model

def get_flower_classification_model():
    return MechineLearningUtil.load_model("./src/ml_model/flower_classification_model.h5")
