import tensorflow as tf
import numpy as np

class MechineLearningUtil:
    
    @staticmethod
    def load_model(filePath : str):
        model = tf.keras.models.load_model(filePath)
        return model
        
        
    @staticmethod
    def resize_image_metrix(imgMatrix, h : int, w : int):
        res = tf.image.resize(imgMatrix,[h,w])
        return res