import imp


import src.ml_model as ml_model
import numpy as np
from .model import *

class FlowerClassificationUsecase:
    
    class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']
    
    def predict_pict(self, data) -> PredictResponse:
        flower_classification_model = ml_model.get_flower_classification_model()
        predict_res = flower_classification_model.predict(np.array([data]))
        predict_class = np.argmax(predict_res)
        class_name = self.class_names[predict_class]
        
        predict_map_res = {}
        persentage = ClassesLabelPersentage()
        for i in range(len(self.class_names)):
            predict_map_res[self.class_names[i]] = float(predict_res[0][i])
        
        persentage.daisy = predict_map_res['daisy']
        persentage.dandelion = predict_map_res['dandelion']
        persentage.roses = predict_map_res['roses']
        persentage.sunflowers = predict_map_res['sunflowers']
        persentage.tulips = predict_map_res['tulips']
        return PredictResponse(predict_result=persentage, class_name = class_name)
        