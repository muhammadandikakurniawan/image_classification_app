from typing import Any
from pydantic import BaseModel

class ClassesLabelPersentage(BaseModel):
    daisy: Any
    dandelion: Any
    roses: Any
    sunflowers: Any
    tulips: Any

class PredictResponse(BaseModel):
    predict_result : ClassesLabelPersentage
    class_name : str
    