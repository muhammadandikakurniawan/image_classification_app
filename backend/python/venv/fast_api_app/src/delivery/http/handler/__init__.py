from fastapi import FastAPI
from fastapi.routing import APIRouter

from . import flower_classification_handler

def _build_router() -> FastAPI :
    rt = APIRouter()
    rt.include_router(flower_classification_handler.router, prefix="/flower-classification", tags=["FlowerClassification"]) 
    return rt

router = _build_router()