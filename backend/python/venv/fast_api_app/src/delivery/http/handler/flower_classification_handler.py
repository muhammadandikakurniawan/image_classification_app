from fastapi import Request, File, UploadFile
from fastapi.responses import JSONResponse  # type: ignore
from fastapi.routing import APIRouter
from numpy.lib.type_check import imag
from src.container import container
from pydantic import BaseModel
from PIL import Image
from io import BytesIO
import numpy as np
import tensorflow as tf
from src.usecase.model import PredictResponse
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

router = APIRouter(default_response_class=JSONResponse)

flower_classification_usecase = container.dependencies.flower_classification_usecase

class PredictPictReq(BaseModel):
    data: None

@router.post("/predict-pict", response_model=PredictResponse)
async def preidct_pict(file: UploadFile = File(...)):
   
    imageFile = await file.read()
    image = Image.open(BytesIO(imageFile))
    imageMatrix = np.array(image)
    imageMatrix = tf.image.resize(imageMatrix, [180,180])
    result = flower_classification_usecase.predict_pict(imageMatrix)
    return result