from fastapi import FastAPI
import uvicorn
from src.delivery.http import handler

web_app = handler.router

def run_web_server() -> FastAPI:
    uvicorn.run(
        "src.delivery.server:web_app",
        port=2404,
        host="192.168.0.19",
        reload=True,
    )
