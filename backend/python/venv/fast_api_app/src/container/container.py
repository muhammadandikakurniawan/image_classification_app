from tensorflow.python.framework.ops import container
from src.usecase import flower_classification_usecase


class Container :
    flower_classification_usecase : flower_classification_usecase.FlowerClassificationUsecase
    

def build_dependencies():
    container = Container
    container.flower_classification_usecase = flower_classification_usecase.FlowerClassificationUsecase()
    return container

dependencies = build_dependencies()